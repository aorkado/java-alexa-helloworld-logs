## If you like this project, please FOLLOW ME on LINKEDIN :)

https://www.linkedin.com/in/javierochoaorihuel/

---

# HelloWorld - Logging Requests and Responses

An improved version of the java-alexa-hellowoworld project. 

It presents a way to log both the request and response "the right way", meaning there is no loss of responses, as it may happen using "interceptors".
 
## Notes:
There seem to be lots of posts and articles that tell how to log out request and response data using "interceptors", but I would advise NOT to use "interceptors" since the ResponseInterceptors only intercept responses from RequestHandlers, leaving out all possible ExceptionHandlers your code may have. 

When a skill is built and initialize using ASK, the pipeline used to process the request is as follows: RequestInterceptor > RequestHandler > ResponseInterceptor > ExceptionHandler.

You can think of the ExceptionHandler as the “finally” of a “try-catch” block.

The ExceptionHandler will be called in case the exception happens and will provide with a proper response to your users, but it won’t be passed through the ResponseInterceptor.

In this code you can find a custom implementation of a RequestStreamHandler which allows us to spy on the actual real input and output stream and log that information out.

## Contact
For any questions or comments, you can contact me via linkedin => https://www.linkedin.com/in/javierochoaorihuel/


