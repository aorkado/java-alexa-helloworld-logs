package com.aorkado.helloworld.tools.builders;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import com.aorkado.helloworld.AbstractTest;
import com.aorkado.helloworld.requesthandlers.matchers.AbstractMatcher;
import com.aorkado.helloworld.requesthandlers.models.AbstractHandlerModel;
import jodd.petite.PetiteContainer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class IntentHandlerBuilderTest extends AbstractTest {

  private static final String SKILL_ID = "1234567890";

  private PetiteContainer petiteContainer;
  private IntentHandlerBuilder builder;

  @Before
  public void before() {
    petiteContainer = mock(PetiteContainer.class);

    builder = new IntentHandlerBuilder();
    builder.setPetiteContainer(petiteContainer);
  }

  @Test
  public void build() {
    builder.build(
        AbstractMatcher.class,
        AbstractHandlerModel.class
    );

    verify(petiteContainer).getBean(AbstractMatcher.class);
    verify(petiteContainer).getBean(AbstractHandlerModel.class);
    verifyNoMoreInteractions(petiteContainer);
  }
}
