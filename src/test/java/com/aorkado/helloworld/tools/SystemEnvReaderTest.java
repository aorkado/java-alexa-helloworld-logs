package com.aorkado.helloworld.tools;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import com.aorkado.helloworld.AbstractTest;
import com.aorkado.helloworld.tools.SystemEnvReader.SystemEnvKeys;
import com.aorkado.helloworld.utils.SystemUtils;
import jodd.util.StringPool;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SystemEnvReaderTest extends AbstractTest {

  private static final String VARIABLE = "FOOBAR";

  private SystemUtils systemUtils;
  private SystemEnvReader reader;

  @Before
  public void before() {
    systemUtils = mock(SystemUtils.class);
    doReturn(VARIABLE).when(systemUtils).getEnvironmentVariable(anyString(), anyString());

    reader = new SystemEnvReader();
    reader.setSystemUtils(systemUtils);
  }

  @Test
  public void skillId() {
    String result = reader.skillId();

    Assert.assertEquals(VARIABLE, result);

    verify(systemUtils).getEnvironmentVariable(SystemEnvKeys.SKILL_ID.name(), StringPool.EMPTY);
    verifyNoMoreInteractions(systemUtils);
  }
}
