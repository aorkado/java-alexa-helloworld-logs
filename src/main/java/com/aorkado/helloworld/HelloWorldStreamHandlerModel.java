package com.aorkado.helloworld;

import com.amazon.ask.Skill;
import com.amazon.ask.request.impl.BaseSkillRequest;
import com.amazon.ask.response.SkillResponse;
import com.aorkado.helloworld.tools.builders.SkillBuilder;
import com.aorkado.helloworld.utils.IOUtils;
import java.io.InputStream;
import java.io.OutputStream;
import jodd.exception.ExceptionUtil;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PetiteBean
public class HelloWorldStreamHandlerModel {

  private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldStreamHandlerModel.class);

  private SkillBuilder skillBuilder;
  private IOUtils ioUtils;

  @PetiteInject
  public void setIoUtils(IOUtils ioUtils) {
    this.ioUtils = ioUtils;
  }

  @PetiteInject
  public void setSkillBuilder(SkillBuilder skillBuilder) {
    this.skillBuilder = skillBuilder;
  }

  public void handleRequest(InputStream inputStream, OutputStream outputStream) {
    try {
      byte[] inputBytes = ioUtils.toByteArray(inputStream);

      logBytes(inputBytes);

      Skill skill = skillBuilder.build();
      SkillResponse response = skill.execute(new BaseSkillRequest(inputBytes));

      if (null != response && response.isPresent()) {
        logBytes(response.getRawResponse());
        ioUtils.write(response.getRawResponse(), outputStream);
      }
    } catch (Exception e) {
      LOGGER.error(ExceptionUtil.exceptionStackTraceToString(e));
    }
  }

  private void logBytes(byte[] bytes) {
    try {
      LOGGER.info(ioUtils.toString(bytes));
    } catch (Exception e) {
      LOGGER.error(ExceptionUtil.exceptionStackTraceToString(e));
    }
  }
}