package com.aorkado.helloworld;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import java.io.InputStream;
import java.io.OutputStream;
import jodd.exception.ExceptionUtil;
import jodd.petite.AutomagicPetiteConfigurator;
import jodd.petite.PetiteContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloWorldStreamHandler implements RequestStreamHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldStreamHandler.class);

  private static PetiteContainer petiteContainer = null;

  public HelloWorldStreamHandler() {
    try {
      if (null == petiteContainer) {
        initPetiteContainer();
      }
    } catch (Exception e) {
      LOGGER.error(ExceptionUtil.exceptionStackTraceToString(e));
    }
  }

  private static void initPetiteContainer() {
    petiteContainer = new PetiteContainer();
    petiteContainer.addSelf();

    AutomagicPetiteConfigurator configurator = new AutomagicPetiteConfigurator(petiteContainer);
    configurator.configure();
  }

  @Override
  public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) {
    try {
      HelloWorldStreamHandlerModel handlerModel = petiteContainer
          .getBean(HelloWorldStreamHandlerModel.class);
      handlerModel.handleRequest(inputStream, outputStream);
    } catch (Exception e) {
      LOGGER.error(ExceptionUtil.exceptionStackTraceToString(e));
    }
  }
}