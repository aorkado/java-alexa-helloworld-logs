package com.aorkado.helloworld.requesthandlers.matchers;


import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.aorkado.helloworld.Constants;
import jodd.exception.ExceptionUtil;
import jodd.petite.meta.PetiteBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PetiteBean
public class FallbackIntentMatcher extends AbstractMatcher {

  private static final Logger LOGGER = LoggerFactory.getLogger(FallbackIntentMatcher.class);

  public boolean match(HandlerInput handlerInput) {
    try {
      return handlerInput.matches(predicatesUtils.intentName(Constants.FALLBACK_INTENT));
    } catch (Exception e) {
      LOGGER.error(ExceptionUtil.exceptionStackTraceToString(e));
      return false;
    }
  }
}