package com.aorkado.helloworld.tools.builders;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.request.handler.GenericRequestHandler;
import com.aorkado.helloworld.requesthandlers.matchers.CancelIntentMatcher;
import com.aorkado.helloworld.requesthandlers.matchers.FallbackIntentMatcher;
import com.aorkado.helloworld.requesthandlers.matchers.HelloWorldIntentMatcher;
import com.aorkado.helloworld.requesthandlers.matchers.HelpIntentMatcher;
import com.aorkado.helloworld.requesthandlers.matchers.LaunchRequestMatcher;
import com.aorkado.helloworld.requesthandlers.matchers.SessionEndedRequestMatcher;
import com.aorkado.helloworld.requesthandlers.matchers.StopIntentMatcher;
import com.aorkado.helloworld.requesthandlers.models.CancelIntentHandlerModel;
import com.aorkado.helloworld.requesthandlers.models.FallbackIntentHandlerModel;
import com.aorkado.helloworld.requesthandlers.models.HelloWorldIntentHandlerModel;
import com.aorkado.helloworld.requesthandlers.models.HelpIntentHandlerModel;
import com.aorkado.helloworld.requesthandlers.models.LaunchRequestHandlerModel;
import com.aorkado.helloworld.requesthandlers.models.SessionEndedRequestHandlerModel;
import com.aorkado.helloworld.requesthandlers.models.StopIntentHandlerModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;

@PetiteBean
public class RequestHandlerListBuilder {

  private IntentHandlerBuilder intentHandlerBuilder;

  @PetiteInject
  public void setIntentHandlerBuilder(
      IntentHandlerBuilder intentHandlerBuilder) {
    this.intentHandlerBuilder = intentHandlerBuilder;
  }

  public List<GenericRequestHandler<HandlerInput, Optional<Response>>> build() {
    List<GenericRequestHandler<HandlerInput, Optional<Response>>> l = new ArrayList<>();
    l.add(buildLaunchRequestIntentHandler());
    l.add(buildSessionEndedRequestHandler());
    l.add(buildHelpIntentHandler());
    l.add(buildCancelIntentHandler());
    l.add(buildStopIntentHandler());
    l.add(buildFallbackIntentHandler());
    l.add(buildHelloWorldIntentHandler());
    return l;
  }

  private GenericRequestHandler<HandlerInput, Optional<Response>> buildHelloWorldIntentHandler() {
    return intentHandlerBuilder.build(
        HelloWorldIntentMatcher.class,
        HelloWorldIntentHandlerModel.class
    );
  }

  private GenericRequestHandler<HandlerInput, Optional<Response>> buildFallbackIntentHandler() {
    return intentHandlerBuilder.build(
        FallbackIntentMatcher.class,
        FallbackIntentHandlerModel.class
    );
  }

  private GenericRequestHandler<HandlerInput, Optional<Response>> buildStopIntentHandler() {
    return intentHandlerBuilder.build(
        StopIntentMatcher.class,
        StopIntentHandlerModel.class
    );
  }

  private GenericRequestHandler<HandlerInput, Optional<Response>> buildCancelIntentHandler() {
    return intentHandlerBuilder.build(
        CancelIntentMatcher.class,
        CancelIntentHandlerModel.class
    );
  }

  private GenericRequestHandler<HandlerInput, Optional<Response>> buildHelpIntentHandler() {
    return intentHandlerBuilder.build(
        HelpIntentMatcher.class,
        HelpIntentHandlerModel.class
    );
  }

  private GenericRequestHandler<HandlerInput, Optional<Response>> buildSessionEndedRequestHandler() {
    return intentHandlerBuilder.build(
        SessionEndedRequestMatcher.class,
        SessionEndedRequestHandlerModel.class
    );
  }

  private GenericRequestHandler<HandlerInput, Optional<Response>> buildLaunchRequestIntentHandler() {
    return intentHandlerBuilder.build(
        LaunchRequestMatcher.class,
        LaunchRequestHandlerModel.class
    );
  }
}
