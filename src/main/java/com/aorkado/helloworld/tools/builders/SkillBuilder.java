package com.aorkado.helloworld.tools.builders;

import com.amazon.ask.Skill;
import com.amazon.ask.builder.StandardSkillBuilder;
import com.aorkado.helloworld.tools.SystemEnvReader;
import com.aorkado.helloworld.utils.SkillsUtils;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;

@PetiteBean
public class SkillBuilder {

  private SkillsUtils skillsUtils;
  private SystemEnvReader systemEnvReader;
  private RequestHandlerListBuilder requestHandlerListBuilder;

  @PetiteInject
  public void setSkillsUtils(SkillsUtils skillsUtils) {
    this.skillsUtils = skillsUtils;
  }

  @PetiteInject
  public void setSystemEnvReader(SystemEnvReader systemEnvReader) {
    this.systemEnvReader = systemEnvReader;
  }

  @PetiteInject
  public void setRequestHandlerListBuilder(
      RequestHandlerListBuilder requestHandlerListBuilder) {
    this.requestHandlerListBuilder = requestHandlerListBuilder;
  }

  public Skill build() {
    StandardSkillBuilder standardSkillBuilder = skillsUtils.standard();
    standardSkillBuilder.withSkillId(systemEnvReader.skillId());
    standardSkillBuilder.addRequestHandlers(requestHandlerListBuilder.build());
    return standardSkillBuilder.build();
  }
}
